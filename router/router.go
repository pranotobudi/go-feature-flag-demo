package router

import (
	"net/http"

	"gitlab.com/pranotobudi/go-feature-flag-demo/email"
)

type Router struct {
	emailController email.IEmailController
}

func NewRouter(emailController email.IEmailController) *Router {
	return &Router{
		emailController: emailController,
	}
}
func (r *Router) InitRouter() http.Handler {
	router := http.NewServeMux()
	// router.HandleFunc("/", hello.HelloServer)
	router.HandleFunc("/email", r.emailController.SendEmailController)
	return router
}
