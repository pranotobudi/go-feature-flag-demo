package router_test

import (
	"bytes"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/pranotobudi/go-feature-flag-demo/router"
)

type EmailControllerMock struct {
	mock.Mock
}

func (ec *EmailControllerMock) SendEmailController(w http.ResponseWriter, req *http.Request) {
	ec.Called(w, req)
	// args := ec.Called(w, req)
	// return args.Error(0)
}
func TestNewRouter(t *testing.T) {
	emailControllerMock := new(EmailControllerMock)
	call := emailControllerMock.On("SendEmailController", mock.Anything, mock.Anything)
	router := router.NewRouter(emailControllerMock)
	srv := httptest.NewServer(router.InitRouter())

	input := []byte(`{"body":"Email Body"}`)
	http.Post(fmt.Sprintf("%s/email", srv.URL), "application/json", bytes.NewBuffer(input))
	// res, err := http.Post(fmt.Sprintf("%s/email", srv.URL), "application/json", bytes.NewBuffer(input))
	// body, err := ioutil.ReadAll(res.Body)

	// these two is the same objective, I just want to put it all together
	emailControllerMock.AssertCalled(t, "SendEmailController", mock.Anything, mock.Anything)
	assert.Equal(t, "SendEmailController", call.Method)
	// assert.Nil(t, err, "Could not send GET Request: %v", err)
	// assert.Equal(t, http.StatusOK, res.StatusCode, "Expected status OK: got %v", res.StatusCode)
	// assert.Equal(t, "Email sent - success", string(body), "Expected response: Email sent")
}

func TestNewRouter2(t *testing.T) {
	emailControllerMock := new(EmailControllerMock)
	call := emailControllerMock.On("SendEmailController", mock.Anything, mock.Anything)
	router := router.NewRouter(emailControllerMock)
	srv := httptest.NewServer(router.InitRouter())

	input := []byte(`{"body":"Email Body"}`)
	http.Post(fmt.Sprintf("%s/email", srv.URL), "application/json", bytes.NewBuffer(input))
	// res, err := http.Post(fmt.Sprintf("%s/email", srv.URL), "application/json", bytes.NewBuffer(input))
	// body, err := ioutil.ReadAll(res.Body)

	// these two is the same objective, I just want to put it all together
	emailControllerMock.AssertCalled(t, "SendEmailController", mock.Anything, mock.Anything)
	assert.Equal(t, "SendEmailController", call.Method)
	// assert.Nil(t, err, "Could not send GET Request: %v", err)
	// assert.Equal(t, http.StatusOK, res.StatusCode, "expected status: OK, got: %v", res.StatusCode)
	// assert.Equal(t, "Email sent - success", string(body), "Expected response: Email sent")
}

func TestNewRouter3(t *testing.T) {
	emailControllerMock := new(EmailControllerMock)
	call := emailControllerMock.On("SendEmailController", mock.Anything, mock.Anything)
	router := router.NewRouter(emailControllerMock)
	srv := httptest.NewServer(router.InitRouter())

	input := []byte(`{"body":"Email Body"}`)
	http.Post(fmt.Sprintf("%s/email", srv.URL), "application/json", bytes.NewBuffer(input))
	// res, err := http.Post(fmt.Sprintf("%s/email", srv.URL), "application/json", bytes.NewBuffer(input))
	// body, err := ioutil.ReadAll(res.Body)

	// these two is the same objective, I just want to put it all together
	emailControllerMock.AssertCalled(t, "SendEmailController", mock.Anything, mock.Anything)
	assert.Equal(t, "SendEmailController", call.Method)
	// assert.Nil(t, err, "Could not send GET Request: %v", err)
	// assert.Equal(t, http.StatusOK, res.StatusCode, "Expected status: OK, got: %v", res.StatusCode)
	// assert.Equal(t, "Email sent - success", string(body), "Expected response: Email sent")
}

func TestNewRouter4(t *testing.T) {
	tt := []struct {
		name     string
		urlPath  string
		codeWant int
		bodyWant string
	}{
		{"email", "email", http.StatusOK, "Email sent - success"},
		// {"hello", "", http.StatusOK, "Hi Bro"},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			emailControllerMock := new(EmailControllerMock)
			call := emailControllerMock.On("SendEmailController", mock.Anything, mock.Anything)
			router := router.NewRouter(emailControllerMock)
			srv := httptest.NewServer(router.InitRouter())

			input := []byte(`{"body":"Email Body"}`)
			http.Post(fmt.Sprintf("%s/email", srv.URL), "application/json", bytes.NewBuffer(input))
			// res, err := http.Post(fmt.Sprintf("%s/email", srv.URL), "application/json", bytes.NewBuffer(input))
			// body, err := ioutil.ReadAll(res.Body)

			// these two is the same objective, I just want to put it all together
			emailControllerMock.AssertCalled(t, "SendEmailController", mock.Anything, mock.Anything)
			assert.Equal(t, "SendEmailController", call.Method)
			// assert.Nil(t, err, "Could not send GET Request: %v", err)
			// assert.Equal(t, tc.codeWant, res.StatusCode, "Expected status: %d, got: %v", tc.codeWant, res.StatusCode)
			// assert.Equal(t, tc.bodyWant, string(body), fmt.Sprintf("Expected response: %s, got: %s", tc.bodyWant, string(body)))
		})
	}
}

func TestNewRouter5(t *testing.T) {
	tt := []struct {
		name     string
		urlPath  string
		codeWant int
		bodyWant string
	}{
		{"email", "email", http.StatusOK, "Email sent - success"},
		// {"hello", "", http.StatusOK, "Hi Bro"},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			emailControllerMock := new(EmailControllerMock)
			call := emailControllerMock.On("SendEmailController", mock.Anything, mock.Anything)
			router := router.NewRouter(emailControllerMock)
			srv := httptest.NewServer(router.InitRouter())

			input := []byte(`{"body":"Email Body"}`)
			http.Post(fmt.Sprintf("%s/email", srv.URL), "application/json", bytes.NewBuffer(input))
			// res, err := http.Post(fmt.Sprintf("%s/email", srv.URL), "application/json", bytes.NewBuffer(input))
			// body, err := ioutil.ReadAll(res.Body)

			// these two is the same objective, I just want to put it all together
			emailControllerMock.AssertCalled(t, "SendEmailController", mock.Anything, mock.Anything)
			assert.Equal(t, "SendEmailController", call.Method)

			// assert.Nil(t, err, "Can't Send GET Request: %s", err)
			// assert.Equal(t, tc.codeWant, res.StatusCode, "expected: %d, get: %d", tc.codeWant, res.StatusCode)
			// assert.Equal(t, tc.bodyWant, string(body), fmt.Sprintf("Expected Response: %s", tc.bodyWant))
		})
	}

}
