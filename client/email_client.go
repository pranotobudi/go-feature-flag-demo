package client

import (
	"gitlab.com/pranotobudi/go-feature-flag-demo/email"
)

type EmailClient struct {
	emailService email.IEmailService
	Name         string
	Body         string
}

func NewEmailClient(name string, emailService email.IEmailService) *EmailClient {
	return &EmailClient{
		emailService: emailService,
		Name:         name,
	}
}

func (ec *EmailClient) SetBody(body string) {
	ec.Body = body
}

func (ec *EmailClient) SendEmail() {
	ec.emailService.SendEmail(ec.Name + ":" + ec.Body)
}
