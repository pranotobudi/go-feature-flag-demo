package client_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/pranotobudi/go-feature-flag-demo/client"
)

type EmailServiceMock struct {
	mock.Mock
	recorder string
}

func (m *EmailServiceMock) SendEmail(body string) string {
	m.recorder = body
	return "ok"
}

func TestSetBody(t *testing.T) {
	emailServiceMock := new(EmailServiceMock)
	emailClient := client.NewEmailClient("client1", emailServiceMock)

	emailClient.SetBody("email body")

	assert.Equal(t, "email body", emailClient.Body)
}

func TestSendEmail(t *testing.T) {
	emailServiceMock := new(EmailServiceMock)
	emailClient := client.NewEmailClient("client1", emailServiceMock)

	emailClient.SetBody("email body")
	emailClient.SendEmail()

	assert.Equal(t, "client1:email body", emailServiceMock.recorder)

}
