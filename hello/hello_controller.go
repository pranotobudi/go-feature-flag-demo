package hello

import (
	"io"
	"net/http"

	"github.com/Unleash/unleash-client-go/v3"
	"gitlab.com/pranotobudi/go-feature-flag-demo/client"
	"gitlab.com/pranotobudi/go-feature-flag-demo/database"
	"gitlab.com/pranotobudi/go-feature-flag-demo/email"
)

type metricsInterface struct {
}

func init() {
	unleash.Initialize(
		unleash.WithUrl("https://gitlab.com/api/v4/feature_flags/unleash/36721584"),
		unleash.WithInstanceId("SmBpo92pe-CrXo9zUBR-"),
		unleash.WithAppName("production"), // Set to the running environment of your application
		unleash.WithListener(&metricsInterface{}),
	)
}

func HelloServer(w http.ResponseWriter, req *http.Request) {
	postgres := database.GetDB()
	repo := email.NewEmailRepository("DBName1", postgres)
	emailServiceOld := email.NewEmailServiceOld("EmailServiceOld", repo)
	emailServiceNew := email.NewEmailServiceNew("EmailServiceNew")
	var emailClient *client.EmailClient

	if unleash.IsEnabled("ffdemo") {
		io.WriteString(w, "Feature enabled\n")
		emailClient = client.NewEmailClient("clientNew", emailServiceNew)
	} else {
		io.WriteString(w, "hello, world!\n")
		emailClient = client.NewEmailClient("clientOld", emailServiceOld)
	}
	emailClient.SetBody("email body")
	emailClient.SendEmail()
	w.Write([]byte("Hi Bro"))

}
