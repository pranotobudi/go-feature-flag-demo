package email_test

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/pranotobudi/go-feature-flag-demo/email"
	"gitlab.com/pranotobudi/go-feature-flag-demo/router"
)

type EmailServiceMock struct {
	mock.Mock
}

func (es *EmailServiceMock) SendEmail(body string) string {
	args := es.Called(body)
	return args.String(0)
}

func TestSendEmailController(t *testing.T) {
	// mock the service layer
	emailServiceMock := new(EmailServiceMock)
	emailServiceMock.On("SendEmail", mock.Anything).Return("Test Success")
	emailController := email.NewEmailController(emailServiceMock)

	// mock the server
	router := router.NewRouter(emailController)
	srv := httptest.NewServer(router.InitRouter())
	rec := httptest.NewRecorder()
	input := []byte(`{"body":"Email Body"}`)
	req, err := http.NewRequest("POST", fmt.Sprintf("%s/email", srv.URL), bytes.NewBuffer(input))

	emailController.SendEmailController(rec, req)
	res := rec.Result()
	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)

	assert.Nil(t, err, "Could not send POST request: %s", err)
	assert.Equal(t, http.StatusOK, res.StatusCode, "expected status: OK, got: %v", res.StatusCode)
	assert.Equal(t, "Email sent - Test Success", string(body), "expected body: Email sent - Test Success, got: %v", res.Body)
}

func TestSendEmailController2(t *testing.T) {
	// mock the service layer
	emailServiceMock := new(EmailServiceMock)
	emailServiceMock.On("SendEmail", mock.Anything).Return("Test Success")
	emailController := email.NewEmailController(emailServiceMock)

	// mock the server
	router := router.NewRouter(emailController)
	srv := httptest.NewServer(router.InitRouter())
	rec := httptest.NewRecorder()
	input := []byte(`{"body":"Email Body"}`)
	req, err := http.NewRequest("POST", fmt.Sprintf("%s/email", srv.URL), bytes.NewBuffer(input))

	emailController.SendEmailController(rec, req)
	res := rec.Result()
	body, _ := ioutil.ReadAll(res.Body)

	assert.Nil(t, err, "Could not send GET request: %v", err)
	assert.Equal(t, http.StatusOK, res.StatusCode, "expected status: OK, got %v", res.StatusCode)
	assert.Equal(t, "Email sent - Test Success", string(body), "expected body: Email sent - Test Success, got: %v", string(body))
}

func TestSendEmailController3(t *testing.T) {
	// mock the service layer
	emailServiceMock := new(EmailServiceMock)
	emailServiceMock.On("SendEmail", mock.Anything).Return("Test Success")
	emailController := email.NewEmailController(emailServiceMock)

	// mock the server
	router := router.NewRouter(emailController)
	srv := httptest.NewServer(router.InitRouter())
	rec := httptest.NewRecorder()
	input := []byte(`{"body":"Email Body"}`)
	req, err := http.NewRequest("POST", fmt.Sprintf("%s/email", srv.URL), bytes.NewBuffer(input))

	emailController.SendEmailController(rec, req)
	res := rec.Result()
	body, _ := ioutil.ReadAll(res.Body)
	assert.Nil(t, err, "Could not send POST request: %v", err)
	assert.Equal(t, "Email sent - Test Success", string(body), "expected result: Email sent - Test Success, got: %v", string(body))
	assert.Equal(t, http.StatusOK, res.StatusCode, "expected status OK, got: %v", res.StatusCode)
}

func TestSendEmailController4(t *testing.T) {
	tt := []struct {
		name          string
		mockedMethod  string
		emailBody     string
		returnArgs    string
		requestInput  string
		requestMethod string
		codeWant      int
		resultWant    string
	}{
		{"condition1", "SendEmail", "Email Body", "Test Success", `{"body":"Email Body"}`, "POST", http.StatusOK, "Email sent - Test Success"},
		{"condition2", "SendEmail", "Email Body", "Test Success", `{"body":"Email Body"}`, "POST", http.StatusOK, "Email sent - Test Success"},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			// mock the service layer
			emailServiceMock := new(EmailServiceMock)
			emailServiceMock.On(tc.mockedMethod, mock.Anything).Return(tc.returnArgs)
			emailController := email.NewEmailController(emailServiceMock)

			// mock the server
			router := router.NewRouter(emailController)
			srv := httptest.NewServer(router.InitRouter())
			rec := httptest.NewRecorder()
			input := []byte(`{"body":"Email Body"}`)
			req, err := http.NewRequest("POST", fmt.Sprintf("%s/email", srv.URL), bytes.NewBuffer(input))

			emailController.SendEmailController(rec, req)
			res := rec.Result()
			body, _ := ioutil.ReadAll(res.Body)
			assert.Nil(t, err, "Could not send POST request: %v", err)
			assert.Equal(t, tc.resultWant, string(body), "expected result: Email sent - Test Success, got: %v", string(body))
			assert.Equal(t, tc.codeWant, res.StatusCode, "expected status OK, got: %v", res.StatusCode)
		})
	}
}

func TestSendEmailController5(t *testing.T) {
	tt := []struct {
		name              string
		mockedMethodName  string
		mockedMethodInput string
		returnArgs        string
		requestMethod     string
		urlPath           string
		requestInput      string
		codeWant          int
		resultWant        string
	}{
		{"condition1", "SendEmail", "Email Body", "Test Success", "POST", "email", `{"body":"Email Body"}`, http.StatusOK, "Email sent - Test Success"},
		{"condition2", "SendEmail", "Email Body", "Test Success", "POST", "email", `{"body":"Email Body"}`, http.StatusOK, "Email sent - Test Success"},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			// mock the service layer
			emailServiceMock := new(EmailServiceMock)
			emailServiceMock.On(tc.mockedMethodName, mock.Anything).Return(tc.returnArgs)
			emailController := email.NewEmailController(emailServiceMock)

			// mock the server
			router := router.NewRouter(emailController)
			srv := httptest.NewServer(router.InitRouter())
			rec := httptest.NewRecorder()
			input := []byte(`{"body":"Email Body"}`)
			req, err := http.NewRequest("POST", fmt.Sprintf("%s/email", srv.URL), bytes.NewBuffer(input))

			emailController.SendEmailController(rec, req)
			res := rec.Result()
			b, err := ioutil.ReadAll(res.Body)

			assert.Nil(t, err, "Could not send message: %v", err)
			assert.Equal(t, tc.codeWant, res.StatusCode)
			assert.Equal(t, tc.resultWant, string(b))
		})
	}
}
