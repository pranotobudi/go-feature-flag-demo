package email

import "log"

type EmailServiceOld struct {
	repo IEmailRepository
	name string
}

func NewEmailServiceOld(name string, repo IEmailRepository) *EmailServiceOld {
	return &EmailServiceOld{
		name: name,
		repo: repo,
	}
}

type Email struct {
	Email   string
	Subject string
	Body    string
}

func (es *EmailServiceOld) SendEmail(body string) string {
	log.Println("OldSendEmail send Email" + body)
	email := Email{
		Email:   "pranotobudi.s@gmail.com",
		Subject: "Subject 3",
		Body:    body,
	}
	e, err := es.repo.GetEmail("pranotobudi.s@gmail.com")
	log.Println("service layer - email: ", *e)
	err = es.repo.SaveEmail(email)
	if err != nil {
		return "Failed"
	}
	return "success"
}
