package email

import (
	"encoding/json"
	"log"
	"net/http"
)

type EmailController struct {
	emailService IEmailService
}

func NewEmailController(emailService IEmailService) *EmailController {
	return &EmailController{
		emailService: emailService,
	}
}
func (ec *EmailController) SendEmailController(w http.ResponseWriter, req *http.Request) {
	type RequestBody struct {
		Body string `json:"body"`
	}
	// log.Println("SendEmailController We're here")
	var b RequestBody
	err := json.NewDecoder(req.Body).Decode(&b)
	// body, err := ioutil.ReadAll(req.Body)
	// json.Unmarshal(body, &b)
	log.Printf("SendEmailController %+v", b)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	// log.Println("SendEmailController b: ", b, " b.body: ", b.Body)
	result := ec.emailService.SendEmail(b.Body)
	w.Write([]byte("Email sent - " + result))
}
