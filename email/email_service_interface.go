package email

type IEmailService interface {
	SendEmail(body string) string
}
