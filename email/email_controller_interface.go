package email

import "net/http"

type IEmailController interface {
	SendEmailController(w http.ResponseWriter, req *http.Request)
}
