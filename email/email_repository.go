package email

import (
	"context"
	"fmt"
	"log"

	_ "github.com/lib/pq"
	"gitlab.com/pranotobudi/go-feature-flag-demo/database"
)

type EmailRepository struct {
	postgres *database.PostgresDB
	DBName   string
	recorder string
}

func NewEmailRepository(dbName string, postgres *database.PostgresDB) *EmailRepository {
	return &EmailRepository{
		DBName:   dbName,
		postgres: postgres,
	}
}

func (er *EmailRepository) SaveEmail(email Email) error {
	er.recorder = email.Body

	// update logout mark
	// query := fmt.Sprintf(`
	// UPDATE emails SET body = '%s', subject = '%s' WHERE email = '%s';
	// `, email.Body, email.Subject, email.Email)
	query := "UPDATE emails SET body = ?, subject = ? WHERE email = ?"
	stmt, err := er.postgres.DB.PrepareContext(context.Background(), query)

	log.Println("recorder: ", er.recorder)
	log.Println("SaveEmail query: ", query)
	_, err = stmt.ExecContext(context.Background(), email.Body, email.Subject, email.Email)

	// _, err := er.postgres.DB.ExecContext(context.Background(), query)
	if err != nil {
		log.Println("SQL statement execution failed: ", err)
		return err
	}
	log.Println("Success to execute SQL query, logout mark is true")
	log.Println("Email Saved")

	return nil
}

func (er *EmailRepository) GetEmail(emailAddress string) (*Email, error) {
	query := fmt.Sprintf(`
	SELECT * FROM emails WHERE email='%s';
	`, emailAddress)
	log.Println("query: ", query)

	// execute query
	row := er.postgres.DB.QueryRow(query)

	e := Email{}
	err := row.Scan(&e.Email, &e.Subject, &e.Body)
	if err != nil {
		log.Println("SQL row scan failed, no row matched: ", err)
		return nil, err
	}
	log.Println("product: ", e)

	log.Println("Success to execute SQL query, GetEmail success: ", e)
	return &e, nil

}
