package email

type IEmailRepository interface {
	GetEmail(emailAddress string) (*Email, error)
	SaveEmail(email Email) error
}
