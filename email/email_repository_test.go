package email_test

import (
	"database/sql"
	"log"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/pranotobudi/go-feature-flag-demo/database"
	"gitlab.com/pranotobudi/go-feature-flag-demo/email"
)

func NewDBMock() (*sql.DB, sqlmock.Sqlmock) {
	db, mock, err := sqlmock.New()
	if err != nil {
		log.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}

	return db, mock
}

func TestSaveEmail(t *testing.T) {
	db, mock := NewDBMock()
	defer db.Close()
	postgres := database.PostgresDB{
		DB: db,
	}
	emailRepository := email.NewEmailRepository("dbname1", &postgres)

	e := email.Email{
		Email:   "pranotobudi.s@gmail.com",
		Body:    "Email Body 4",
		Subject: "Subject 4",
	}

	query := "UPDATE emails SET body = \\?, subject = \\? WHERE email = \\?"
	prep := mock.ExpectPrepare(query)
	prep.ExpectExec().WithArgs(e.Body, e.Subject, e.Email).WillReturnResult(sqlmock.NewResult(0, 1))

	err := emailRepository.SaveEmail(e)
	assert.NoError(t, err)
	// if err != nil {
	// 	t.Errorf("error was not expected while updating stats: %s", err)
	// }
}
