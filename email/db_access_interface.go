package email

type IDBAccess interface {
	GetEmail(emailAddress string) (*Email, error)
	SaveEmail(email Email) error
}
