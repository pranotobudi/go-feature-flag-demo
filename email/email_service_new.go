package email

import "log"

type EmailServiceNew struct {
	name string
}

func NewEmailServiceNew(name string) *EmailServiceNew {
	return &EmailServiceNew{
		name: name,
	}
}
func (es *EmailServiceNew) SendEmail(body string) string {
	log.Println("NewSendEmail send Email" + body)
	return "success"
}
