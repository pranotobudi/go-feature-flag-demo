package email_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/pranotobudi/go-feature-flag-demo/email"
)

type EmailRepositoryMock struct {
	mock.Mock
	recorder string
}

func (er *EmailRepositoryMock) SaveEmail(email email.Email) error {
	er.recorder = email.Body
	args := er.Called(email)
	return args.Error(0)
}
func (er *EmailRepositoryMock) GetEmail(emailAddress string) (*email.Email, error) {
	args := er.Called(emailAddress)
	return args.Get(0).(*email.Email), args.Error(1)
}

func TestSendEmail(t *testing.T) {
	tt := []struct {
		name                    string
		mockedMethodName        string
		mockedMethodInput       *email.Email
		mockedMethodReturnValue *email.Email
		emailBody               string
		result                  string
	}{
		{"condition1", "SaveEmail", &email.Email{Email: "pranotobudi.s@gmail.com", Subject: "Subject 4", Body: "Email Body 4"}, &email.Email{Email: "pranotobudi.s@gmail.com", Subject: "Subject 4", Body: "Email Body 4"}, "Email Body 4", "success"},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			emailRepositoryMock := new(EmailRepositoryMock)
			call1 := emailRepositoryMock.On(tc.mockedMethodName, mock.Anything).Return(nil)
			call2 := emailRepositoryMock.On("GetEmail", mock.Anything).Return(tc.mockedMethodReturnValue, nil)
			emailServiceOld := email.NewEmailServiceOld(tc.name, emailRepositoryMock)

			result := emailServiceOld.SendEmail(tc.emailBody)

			assert.Equal(t, "SaveEmail", call1.Method)
			assert.Equal(t, "GetEmail", call2.Method)
			assert.Equal(t, tc.emailBody, emailRepositoryMock.recorder)
			assert.Equal(t, tc.result, result)
		})
	}
}
