package main

import (
	"log"
	"net/http"
	"os"

	"github.com/joho/godotenv"
	"gitlab.com/pranotobudi/go-feature-flag-demo/database"
	"gitlab.com/pranotobudi/go-feature-flag-demo/email"
	"gitlab.com/pranotobudi/go-feature-flag-demo/router"
)

func main() {
	if os.Getenv("APP_ENV") != "production" {
		// executed in development only,
		//for production set those on production environment settings

		// load local env variables to os
		err := godotenv.Load(".env")
		if err != nil {
			log.Println("failed to load .env file")
		}
	}

	postgres := database.GetDB()
	postgres.MigrateDB("./database/init.sql")
	emailRepository := email.NewEmailRepository("DBName1", postgres)
	emailService := email.NewEmailServiceOld("emailService1", emailRepository)
	emailController := email.NewEmailController(emailService)
	router := router.NewRouter(emailController)
	// initUnleash()
	// http.HandleFunc("/", helloServer)
	// log.Fatal(http.ListenAndServe(":8080", nil))
	log.Fatal(http.ListenAndServe(":8080", router.InitRouter()))
}
