module gitlab.com/pranotobudi/go-feature-flag-demo

go 1.16

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0 // indirect
	github.com/Unleash/unleash-client-go/v3 v3.5.0
	github.com/joho/godotenv v1.4.0
	github.com/lib/pq v1.10.6
	github.com/stretchr/testify v1.7.1
)
